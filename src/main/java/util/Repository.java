package util;

import customer.Customer;
import customer.CustomerId;
import token.Token;
import token.TokenId;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface Repository {
    boolean containsCustomer(CustomerId customerId);
    boolean containsToken(TokenId tokenId);
    void createCustomer(Customer customer);
    void createToken(Token token);
    void updateCustomer(Customer customer);
    void updateToken(Token token);
    Optional<Customer> getCustomer(CustomerId customerId);
    Optional<Token> getToken(TokenId tokenId);
    List<Token> getTokens();
}
