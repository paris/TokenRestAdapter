package util;

import customer.Customer;
import customer.CustomerId;
import token.Token;
import token.TokenId;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InMemoryRepository implements Repository {
    private List<Customer> customers;
    private List<Token> tokens;

    public InMemoryRepository() {
        customers = new ArrayList<>();
        tokens = new ArrayList<>();

        Customer customer1 = new Customer(new CustomerId("customer1"));
        Customer customer2 = new Customer(new CustomerId("customer2"));
        createCustomer(customer1);
        createCustomer(customer2);
    }

    @Override
    public boolean containsCustomer(CustomerId customerId) {
        return customers.stream().anyMatch(c -> c.getCustomerId().equals(customerId));
    }

    @Override
    public boolean containsToken(TokenId tokenId) {
        return tokens.stream().anyMatch(t -> t.getTokenId().equals(tokenId));
    }

    @Override
    public void createCustomer(Customer customer) {
        customers.add(customer);
    }

    @Override
    public void createToken(Token token) {
        tokens.add(token);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customers.stream().filter(c -> c.equals(customer)).collect(Collectors.toList());
    }

    @Override
    public void updateToken(Token token) {
        tokens.stream().filter(t -> t.equals(token)).collect(Collectors.toList());
    }

    @Override
    public Optional<Customer> getCustomer(CustomerId customerId) {
        return customers.stream().filter(c -> c.getCustomerId().equals(customerId)).findFirst();
    }

    @Override
    public Optional<Token> getToken(TokenId tokenId) {
        return tokens.stream().filter(t -> t.getTokenId().equals(tokenId)).findFirst();
    }

    @Override
    public List<Token> getTokens() {
        return tokens;
    }
}
