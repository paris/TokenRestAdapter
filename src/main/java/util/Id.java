package util;

import org.apache.commons.lang.builder.HashCodeBuilder;

public abstract class Id {
    private String id;

    public Id() {}

    public Id(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        return id.equals(((Id) obj).getId());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).
                append(id).
                toHashCode();
    }

    /**
     * Returns the Id.
     * @return the id.
     */
    public String getId() {
        return id;
    }
}
