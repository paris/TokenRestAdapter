package com.paris.services.tokenrestadapter.rest;

import customer.Customer;
import customer.CustomerId;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/customers")
public class CustomersResource {

    @Path("/{customerId}")
    @GET
    @Produces("application/json")
    public Customer getCustomer(@PathParam("customerId") String customerId) {
        try {
            return TokensResource.tokenService.getCustomer(new CustomerId(customerId)).get();
        } catch (Exception e) {
            throw new BadRequestException(Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build());
        }
    }
}
