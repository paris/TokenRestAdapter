package com.paris.services.tokenrestadapter.rest;

import customer.Customer;
import customer.CustomerId;
import token.Token;
import token.TokenService;
import util.InMemoryRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tokens")
public class TokensResource {
    public static TokenService tokenService = new TokenService(new InMemoryRepository());

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<Token> requestTokens(Customer customer, @QueryParam("number") int number) {
        try {
            return tokenService.requestTokens(customer, number);
        } catch (Exception e) {
            throw new BadRequestException(Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build());
        }
    }

    @GET
    @Produces("application/json")
    public List<Token> getTokens() {
        return tokenService.getTokens();
    }
}
