package customer;

import token.Token;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private CustomerId customerId;
    private List<Token> tokens;

    /**
     * Default Customer Constructor
     */
    public Customer() {}

    /**
     * Customer Constructor
     * @param customerId
     */
    public Customer(CustomerId customerId) {
        this.customerId = customerId;
        this.tokens = new ArrayList<>();
    }

    /**
     * Customer Constructor
     * @param customer
     */
    public Customer(Customer customer) {
        this.customerId = customer.getCustomerId();
        this.tokens = new ArrayList<>();
    }

    /**
     * Returns the customers ID
     * @return CustomerId This returns the customer's ID.
     */
    public CustomerId getCustomerId() {
        return customerId;
    }

    /**
     * Returns the customers tokens
     * @return List<Token> This returns the customers tokens.
     */
    public List<Token> getTokens() {
        return tokens;
    }

    /**
     * This methods adds a list of tokens to the customer
     * @param tokens
     */
    public void addTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
