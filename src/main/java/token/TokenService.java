package token;

import customer.Customer;
import customer.CustomerId;
import util.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class TokenService {
    private Repository repository;

    public TokenService(Repository repository) {
        this.repository = repository;
    }

    public List<Token> requestTokens(Customer customer, int number) throws Exception {
        if (number < 1 || number > 5) {
            throw new Exception("Cannot create tokens");
        }

        if (!repository.containsCustomer(customer.getCustomerId())) {
            Customer c = new Customer(customer);
            repository.createCustomer(c);
        }


        Customer c = repository.getCustomer(customer.getCustomerId()).get();
        if (numberOfUnusedTokens(c.getTokens()) > 1) {
            throw new Exception("Cannot have more than 2 unused tokens");
        }

        List<Token> tokens = generateTokens(number);
        c.addTokens(tokens);
        repository.updateCustomer(customer);
        /*
        List<Token> testList = new ArrayList<>();
        testList.add(new Token(new TokenId("tester"), false));
        */
        return tokens;
    }

    public List<Token> generateTokens(int number) {
        List<Token> tokens = new ArrayList<>();

        for (int i = 0; i < number; i++) {
            Token token = new Token(new TokenId("token" + i), false);
            tokens.add(token);
            repository.createToken(token);
        }
        return tokens;
    }

    public void useToken(TokenId tokenId) throws Exception {
        Optional<Token> token = repository.getToken(tokenId);

        if (!token.isPresent()) {
            throw new Exception("Token does not exist");
        }
        if (token.get().isUsed()) {
            throw new Exception("Cannot reuse a used token");
        }
        token.get().use();
        repository.updateToken(token.get());
    }

    public Optional<Token> getToken(TokenId tokenId) throws Exception {
        Optional<Token> token = repository.getToken(tokenId);

        if (!token.isPresent()) {
            throw new Exception("Token does not exist");
        }

        return token;
    }

    public List<Token> getTokens() {
        return repository.getTokens();
    }

    public Optional<Customer> getCustomer(CustomerId customerId) throws Exception {
        Optional<Customer> customer = repository.getCustomer(customerId);

        if (!customer.isPresent()) {
            throw new Exception("Customer does not exist");
        }

        return customer;
    }

    private int numberOfUnusedTokens(List<Token> tokens) {
        int number = 0;
        for (Token token : tokens) {
            if (!token.isUsed()) {
                number++;
            }
        }
        return number;
    }
}
