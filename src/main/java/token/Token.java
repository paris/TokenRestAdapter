package token;

public class Token {
    private TokenId tokenId;
    private boolean used;

    public Token() {}

    public Token(TokenId tokenId, boolean used) {
        this.tokenId = tokenId;
        this.used = used;
    }

    public boolean isUsed() {
        return used;
    }

    public void use() {
        used = true;
    }

    public TokenId getTokenId() {
        return tokenId;
    }
}
